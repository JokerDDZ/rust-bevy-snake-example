use bevy::ecs::system::Command;
use bevy::prelude::*;
use bevy::core::FixedTimestep;

use basic::position::*;
use basic::snake_head::*;
use basic::food::*;
use basic::arena;
use bevy::transform;

mod basic;


fn main() {
    App::new()

        //resource
        .insert_resource(WindowDescriptor{
            title: "Snake!".to_string(),
            width:500.0,
            height:500.00,
            ..Default::default()
        })
        .insert_resource(ClearColor(Color::rgb(0.04, 0.04, 0.04)))
        .insert_resource(SnakeSegments::default())
        .insert_resource(LastTailPosition::default())
        //#resource

        //events
        .add_event::<GameOverEvent>()
        .add_event::<GrowthEvent>()
        //#events

        //startup system 
        .add_startup_system(setup_camera)
        .add_startup_system(spawn_snake)
        .add_startup_system(text_setup)
        //#startup system






        .add_system(
            snake_movement_input
                .label(SnakeMovement::Input)
                .before(SnakeMovement::Movement),
        )
        .add_system(update_FPS)
        .add_system_set(
            SystemSet::new()
                .with_run_criteria(FixedTimestep::step(0.2))
                .with_system(snake_movement.label(SnakeMovement::Movement))
                .with_system(
                    basic::snake_head::snake_eating
                        .label(SnakeMovement::Eating)
                        .after(SnakeMovement::Movement),
                )
                .with_system(
                    snake_growth
                        .label(SnakeMovement::Growth)
                        .after(SnakeMovement::Eating),
                )
        )
        .add_system_set_to_stage(CoreStage::PostUpdate, SystemSet::new().with_system(position_translation).with_system(size_scaling))
        .add_system_set(
            SystemSet::new().with_run_criteria(FixedTimestep::step(1.0))
            .with_system(food_spawner),
        )
        .add_system(game_over.after(SnakeMovement::Movement))
        .add_plugins(DefaultPlugins)
        .run();
}

fn setup_camera(mut commands: Commands) {
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
}


fn size_scaling(windows:Res<Windows>,mut q: Query<(&basic::size::Size,&mut Transform)>){
    let window = windows.get_primary().unwrap();
    
    for (sprite_size,mut transform) in q.iter_mut(){
        transform.scale = Vec3::new(sprite_size.width/arena::ARENA_WIDTH as f32 * window.width() as f32,
    sprite_size.height/arena::ARENA_HEIGHT as f32 * window.height() as f32,1.0);
    }
}

fn position_translation(windows: Res<Windows>, mut q: Query<(&Position, &mut Transform)>) {
    fn convert(pos: f32, bound_window: f32, bound_game: f32) -> f32 {
        let tile_size = bound_window / bound_game;
        pos / bound_game * bound_window - (bound_window / 2.) + (tile_size / 2.)
    }
    let window = windows.get_primary().unwrap();
    for (pos, mut transform) in q.iter_mut() {
        transform.translation = Vec3::new(
            convert(pos.x as f32, window.width() as f32, arena::ARENA_WIDTH as f32),
            convert(pos.y as f32, window.height() as f32, arena::ARENA_HEIGHT as f32),
            0.0,
        );
    }
}

#[derive(Component)]
struct AnimateTranslation;

fn text_setup(mut commands:Commands,asset_server: Res<AssetServer>){
    let font:Handle<Font> = asset_server.load("fonts/Symtext.ttf");

    let text_style = TextStyle {
        font,
        font_size: 20.0,
        color: Color::WHITE,
    };

    let text_alignment = TextAlignment {
        vertical: VerticalAlign::Top,
        horizontal: HorizontalAlign::Left,
    };

    // let trans = Transform{
    //     translation: Vec3::new(0.,75.,1.),
    //     ..Default::default()
    // };
    
    let pos_fps:f32 = 240.0;

    commands.spawn_bundle(Text2dBundle{
        transform:Transform{
            translation: bevy::math::vec3(-pos_fps, pos_fps, 1.0),
            ..Default::default()
        },
        text: Text::with_section("FPS", text_style.clone(), text_alignment),
        ..Default::default()
    }).insert(AnimateTranslation);
}

fn update_FPS(commands: Commands,time:Res<Time>,mut query:Query<&mut Text,With<AnimateTranslation>>){
    let fps = 1.0/time.delta_seconds_f64();

    for mut text in query.iter_mut(){
        text.sections[0].value = format!("{:.2}", fps);
    }
}



#[derive(SystemLabel, Debug, Hash, PartialEq, Eq, Clone)]
pub enum SnakeMovement {
    Input,
    Movement,
    Eating,
    Growth,
}

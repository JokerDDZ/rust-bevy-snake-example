use bevy::prelude::*;
use rand::prelude::random;
use crate::basic::arena;
use crate::basic::size;
use crate::basic::position;

#[derive(Component)]
pub struct Food;

const FOOD_COLOR: Color = Color::rgb(1.0, 0.0, 0.0);

pub fn food_spawner(mut commands: Commands){
    commands
    .spawn_bundle(SpriteBundle{sprite: Sprite{
        color:FOOD_COLOR,
        ..Default::default()},
        ..Default::default()
    })
    .insert(Food)
    .insert(position::Position{
        x:(random::<f32>() * arena::ARENA_WIDTH as f32) as i32,
        y:(random::<f32>() * arena::ARENA_HEIGHT as f32) as i32,
    })
    .insert(size::Size::square(0.8));
}
use bevy::prelude::*;

#[derive(Component,Clone,Copy,PartialEq,Eq)]
pub struct Position{
    pub x:i32,
    pub y:i32,
}